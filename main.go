package main

import (
	"fmt"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/test", testHandler)
	port := readPort()
	println("Test API Port: " + port)
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		panic(err)
	}
}

func testHandler(w http.ResponseWriter, r *http.Request) {
	_, err := fmt.Fprintf(w, "test")
	if err != nil {
		panic(err)
	}
}

func readPort() string {
	port := "8080"

	// if no port arg return default port
	if len(os.Args) < 2 {
		return port
	}

	// else read port arg
	portArg := os.Args[1]

	// and return port arg if it is set
	if portArg != "" {
		return portArg
	}

	// else return default port
	return port
}
