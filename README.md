# Test API
- Returns 200 OK with Message "test"
- URL Schema: `URL:{PORT}/test`
- OS: Linux (amd64)
- Intended for testing purposes (Firewalls, Ports, ...)

## Usage
```sh
test-api PORT

# Example
test-api 6000
```